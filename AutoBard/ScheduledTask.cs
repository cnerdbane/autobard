﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace AutoBard
{
    class ScheduledTask
    {
        internal readonly Action Action;
        internal System.Timers.Timer Timer;
        internal EventHandler TaskComplete;
        internal CancellationToken CancellationToken;
        private CancellationTokenSource cancellationTokenSource;

        public ScheduledTask(Action action, int timeoutMs)
        {
            Action = action;
            Timer = new System.Timers.Timer() { Interval = timeoutMs };
            Timer.Elapsed += TimerElapsed;
            this.cancellationTokenSource = new CancellationTokenSource();
            this.CancellationToken = this.cancellationTokenSource.Token;
        }

        private void TimerElapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            try
            {
                Timer.Stop();
                Timer.Elapsed -= TimerElapsed;
                Timer = null;
            }
            catch (Exception ex) { }

			Action();
            TaskComplete(this, EventArgs.Empty);
            this.cancellationTokenSource.Cancel();
        }
    }
}
