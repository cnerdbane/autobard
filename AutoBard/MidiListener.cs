﻿using NAudio.Midi;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;

namespace AutoBard
{
	public class MidiListener
	{
		private MidiIn midiIn;
		public bool Monitoring { get; private set; } = false;

		public List<string> MidiDevices { get; } = new List<string>();

        public delegate void NoteReceivedEvent(int note, int channel, bool on);
        public event NoteReceivedEvent NoteReceived;

		public void Initialize()
		{
			for (int device = 0; device < MidiIn.NumberOfDevices; device++)
			{
				MidiDevices.Add(MidiIn.DeviceInfo(device).ProductName);
			}
		}

        public bool StartMonitoring(int index)
        {
            if (MidiDevices.Count() <= index || 0 > index)
            {
                return false;
            }

            if (this.midiIn != null)
            {
                this.midiIn.Dispose();
                this.midiIn.MessageReceived -= midiIn_MessageReceived;
                this.midiIn = null;
            }

            if (this.midiIn == null)
            {
                this.midiIn = new MidiIn(index);
                this.midiIn.MessageReceived += midiIn_MessageReceived;
            }

            this.midiIn.Start();
            this.Monitoring = true;

            return true;
        }

        public void StopMonitoring()
        {
            if (this.Monitoring)
            {
                midiIn?.Stop();
                this.Monitoring = false;
            }
        }

        private void midiIn_MessageReceived(object sender, MidiInMessageEventArgs e)
        {
            switch (e.MidiEvent.CommandCode)
            {
                case MidiCommandCode.NoteOn:
                    this.NoteReceived?.Invoke(((NoteEvent)e.MidiEvent).NoteNumber, 1, true);
                    break;

                case MidiCommandCode.NoteOff:
                    this.NoteReceived?.Invoke(((NoteEvent)e.MidiEvent).NoteNumber, 1, false);
                    break;
            }
        }
    }
}
