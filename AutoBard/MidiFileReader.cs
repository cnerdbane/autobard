﻿using NAudio.Midi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace AutoBard
{
	public class MidiFileReader
	{
		public delegate void NoteReceivedEvent(int note, int channel, bool on);
		public event NoteReceivedEvent NoteReceived;

		public delegate void ProgressEvent(decimal progress);
		public event ProgressEvent Progress;

		public delegate void TotalTimeEvent(decimal totalTime);
		public event TotalTimeEvent TotalTime;

		private readonly Scheduler scheduler = new Scheduler();

		public int ForceChannel { get; set; } = 1;

		public async Task PlaySong(string directory, SongArrangment song, CancellationToken cancellationToken)
		{
			double tempo = 1.0;
			double ticksPerMs = 1.0;
			string filepath = song.GetSongFilePath(directory);

			MidiFile midi = new MidiFile(filepath, false);

			decimal totalTimeMs = CalculateTotalTime(midi);
			if (totalTimeMs <= 0) totalTimeMs = 1; // do something stupid to avoid divide by zero later
			this.TotalTime?.Invoke(totalTimeMs);

			long startMilliseconds = -1;

			this.Progress?.Invoke(0);

			List<Task> tasks = new List<Task>();
			long lastProgressUpdateMs = -1000;

			foreach (Channel channel in midi.Events.Select((x, i) => new Channel { Events = x, Index = i }))
			{
				// one task per channel
				tasks.Add(Task.Run(async () =>
				{
					for (int i = 0; i < channel.Events.Count; i++)
					{
						List<NoteOnEvent> chord = new List<NoteOnEvent>();
						MidiEvent firstNote = channel.Events[i];

						long currentMs = GetCurrentTimeMillis();
						if (startMilliseconds == -1) startMilliseconds = currentMs;

						// based on this channel's index, which players will play this note?
						int players = song.EnsembleMatrix[channel.Index];

						// look ahead for notes with the same absolute time
						int j = i;
						for (; j < channel.Events.Count; j++)
						{
							if (cancellationToken.IsCancellationRequested) return;

							MidiEvent note = channel.Events[j];

							// break if next note is in the future
							if (firstNote.AbsoluteTime < note.AbsoluteTime)
							{
								break;
							}

							if (note is TempoEvent)
							{
								tempo = ((TempoEvent)note).Tempo;
								ticksPerMs = tempo * midi.DeltaTicksPerQuarterNote / 60000.0;
							}

							if (note is NoteOnEvent)
							{
								switch (note.CommandCode)
								{
									case MidiCommandCode.NoteOn:
										NoteOnEvent noteOn = (NoteOnEvent)note;

										if (noteOn.OffEvent != null && noteOn.Velocity > 0)
										{
											chord.Add(noteOn);
										}
										break;
								}
							}
						}
						i = j - 1; // restore counter

						// play chord, if any notes were detected
						if (chord.Any())
						{
							NoteOnEvent firstNoteOn = chord.First();

							int delayUntilNextNoteMs = (int)((firstNoteOn.AbsoluteTime / ticksPerMs) - currentMs + startMilliseconds);

							if (delayUntilNextNoteMs >= 0)
							{
								await Task.Delay(delayUntilNextNoteMs);

								foreach (NoteOnEvent note in chord)
								{
									this.NoteReceived?.Invoke(note.NoteNumber, players, true);

									int noteLength = (int)(note.NoteLength / ticksPerMs);
									if (noteLength > 0)
									{
										this.scheduler.Execute(() => this.NoteReceived?.Invoke(note.NoteNumber, players, false), noteLength);
									}
								}

								if (currentMs > lastProgressUpdateMs + 1000)
								{
									decimal progress = (currentMs - startMilliseconds) / totalTimeMs;
									this.Progress?.Invoke(progress);
									lastProgressUpdateMs = currentMs;
								}
							}
							else
							{

							}
						}
					}
				}));
			}

			await Task.WhenAll(tasks);

			this.Progress?.Invoke(0);
		}

		private static long GetCurrentTimeMillis()
		{
			return DateTime.Now.Ticks / TimeSpan.TicksPerMillisecond;
		}

		public string[] GetTrackNames(string directory, SongArrangment song)
		{
			string filepath = song.GetSongFilePath(directory);
			MidiFile midi = new MidiFile(filepath, false);

			string[] trackNames = new string[midi.Events.Count()];

			for (int i = 0; i < midi.Events.Count(); i++)
			{
				IList<MidiEvent> channelEvents = midi.Events[i];

				for (int j = 0; j < channelEvents.Count; j++)
				{
					MidiEvent midiEvent = channelEvents[j];

					if (midiEvent is TextEvent)
					{
						TextEvent textEvent = (TextEvent)midiEvent;
						if (textEvent.MetaEventType == MetaEventType.SequenceTrackName)
						{
							trackNames[i] = textEvent.Text;
							break;
						}
					}
				}
			}

			return trackNames;
		}

		private static decimal CalculateTotalTime(MidiFile mf)
		{
			decimal maxTime = 0;

			// instead of keeping the tempo event itself, and
			// instead of multiplying every time, just keep
			// the current value for microseconds per tick
			decimal currentMicroSecondsPerTick = 0m;

			for (int n = 0; n < mf.Tracks; n++)
			{
				// Have just one collection for both non-note-off and tempo change events
				List<MidiEvent> midiEvents = new List<MidiEvent>();

				foreach (MidiEvent midiEvent in mf.Events[n])
				{
					if (!MidiEvent.IsNoteOff(midiEvent))
					{
						midiEvents.Add(midiEvent);
					}
				}

				// Switch to decimal from float.
				// decimal has 28-29 digits percision
				// while float has only 6-9
				// https://docs.microsoft.com/en-us/dotnet/csharp/language-reference/builtin-types/floating-point-numeric-types

				// Now we have only one collection of both non-note-off and tempo events
				// so we cannot be sure of the size of the time values array.
				// Just employ a List<float>
				List<decimal> eventsTimesArr = new List<decimal>();

				// Keep track of the last absolute time and last real time because
				// tempo events also can occur "between" events
				// which can cause incorrect times when calculated using AbsoluteTime
				decimal lastRealTime = 0m;
				decimal lastAbsoluteTime = 0m;

				for (int i = 0; i < midiEvents.Count; i++)
				{
					MidiEvent midiEvent = midiEvents[i];
					TempoEvent tempoEvent = midiEvent as TempoEvent;

					// Just append to last real time the microseconds passed
					// since the last event (DeltaTime * MicroSecondsPerTick
					if (midiEvent.AbsoluteTime > lastAbsoluteTime)
					{
						lastRealTime += ((decimal)midiEvent.AbsoluteTime - lastAbsoluteTime) * currentMicroSecondsPerTick;
					}

					lastAbsoluteTime = midiEvent.AbsoluteTime;

					if (tempoEvent != null)
					{
						// Recalculate microseconds per tick
						currentMicroSecondsPerTick = (decimal)tempoEvent.MicrosecondsPerQuarterNote / (decimal)mf.DeltaTicksPerQuarterNote;

						// Remove the tempo event to make events and timings match - index-wise
						// Do not add to the eventTimes
						midiEvents.RemoveAt(i);
						i--;
						continue;
					}

					// Add the time to the collection.
					eventsTimesArr.Add(lastRealTime);
				}

				if (lastRealTime > maxTime)
				{
					maxTime = lastRealTime;
				}
			}

			return maxTime / 1000m;
		}
	}
}
