﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NAudio.Midi;

namespace AutoBard
{
	public class Channel
	{
		public IList<MidiEvent> Events { get; set; }
		public int Index { get; set; }
	}
}
