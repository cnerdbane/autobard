Setup:

First set your bard keybinds like this:
![keybinds](img/keybinds.png)

Usage:

Select the directory with your midi files
Choose song
Click "Play"

![autobard](img/autobard.png)

If your song has multiple parts you can use the Ensemble Picker to select what to play.

![ensemblepicker](img/ensemblepicker.png)

Each row contains one part. Each column is a player.
If you are playing solo, only worry about the first column.

Green means play this part, red means don't play. Click each box to toggle, or click the row/column headers to toggle all.
