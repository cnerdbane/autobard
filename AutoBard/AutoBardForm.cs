﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using AutoBard.Enums;
using AutoBard.Extensions;

namespace AutoBard
{
	public partial class AutoBardForm : Form
	{
		private readonly MidiListener midiListener;
		private NotePlayer notePlayer;
		private DataTable songDataSource = new DataTable("song");
		private CancellationTokenSource cancellationTokenSource;
		private CancellationTokenSource afkCancellationTokenSource;
		private bool playing = false;
		private MidiFileReader midiFileReader = new MidiFileReader();
		private decimal songTotalTimeMs = 0;

		public string CurrentDirectory => this.textBox_MidiDirectory.Text;
		public SongArrangment SelectedSong => ((DataRowView)this.dataGridView1.CurrentRow.DataBoundItem).Row.ToObject<SongArrangment>();
		public int PlayerCount => this.comboBox_GameProcesses.Items.Count;
		public string AfkKeybind
		{
			get => this.toolStripTextBox1.Text;
			set => this.toolStripTextBox1.Text = value;
		}
		public bool StopAfterCurrent
		{
			get => this.stopAfterCurrentToolStripMenuItem.Checked;
			private set => this.stopAfterCurrentToolStripMenuItem.Checked = value;
		}
		public bool PlaybackFollowsCursor
		{
			get => this.playbackFollowsCursorToolStripMenuItem.Checked;
			private set => this.playbackFollowsCursorToolStripMenuItem.Checked = value;
		}
		public Playback Playback { get; private set; }

		public AutoBardForm()
		{
			InitializeComponent();

			this.orderToolStripMenuItem.DropDown.Closing += DropDown_Closing;
			this.playbackToolStripMenuItem.DropDown.Closing += this.DropDown_Closing;

			this.textBox_MidiDirectory.Text = @"P:\midi\";
			this.midiListener = new MidiListener();
			this.dataGridView1.DataSource = this.songDataSource;

			this.midiFileReader.NoteReceived += this.MidiFileReader_NoteReceived;
			this.midiFileReader.Progress += this.MidiFileReader_Progress;
			this.midiFileReader.TotalTime += this.MidiFileReader_TotalTime;
		}

		private void Form1_Load(object sender, EventArgs e)
		{
			this.midiListener.Initialize();
			this.button_Monitor.Enabled = false;
			LoadSettings();

			// find available midi hardware input devices
			this.comboBoxMidiInDevices.Items.AddRange(this.midiListener.MidiDevices.ToArray());
			if (comboBoxMidiInDevices.Items.Count > 0)
			{
				comboBoxMidiInDevices.SelectedIndex = 0;
			}

			// find running FFXIV processes
			for (int i = 0; i < GameInput.GameHandles.Length; i++)
			{
				this.comboBox_GameProcesses.Items.Add(i + 1);
			}
			if (comboBox_GameProcesses.Items.Count > 0)
			{
				comboBox_GameProcesses.SelectedIndex = 0;
			}
			if (comboBox_GameProcesses.Items.Count <= 1)
			{
				this.panel_ProcessSelector.Visible = false;
			}

			this.notePlayer = new NotePlayer(this.PlayerCount)
			{
				AutoOctaveShift = this.checkBox_AutoOctave.Checked,
			};

			this.RefreshFileThings();
			this.textBox_MidiDirectory.TextChanged += new System.EventHandler(this.textBox_MidiDirectory_TextChanged);
			this.dataGridView1.SelectionChanged += new System.EventHandler(this.dataGridView1_SelectionChanged);
			this.dataGridView1.Columns["Ensemble"].Tag = (Action<DataGridViewRow>)EnsembleClickHandler;
		}

		private void LoadSettings()
		{
			this.textBox_MidiDirectory.Text = Properties.Settings.Default.MidiDirectory;
			this.AfkKeybind = Properties.Settings.Default.AfkKeybind;
			this.StopAfterCurrent = Properties.Settings.Default.StopAfterCurrent;
			this.PlaybackFollowsCursor = Properties.Settings.Default.PlaybackFollowsCursor;
			this.Playback = (Playback)Properties.Settings.Default.PlaybackMode;
			this.UpdatePlaybackOrderMenuItems();
		}

		private void button_Monitor_Click(object sender, EventArgs e)
		{
			if (this.midiListener.Monitoring)
			{
				this.midiListener.StopMonitoring();
				this.midiListener.NoteReceived -= this.notePlayer.PlayNote;
				this.button_Monitor.Text = "Monitor";
			}
			else
			{
				this.midiListener.StartMonitoring(comboBoxMidiInDevices.SelectedIndex);
				this.midiListener.NoteReceived += this.notePlayer.PlayNote;
				this.button_Monitor.Text = "Stop";
			}
		}

		private async void button_Play_Click(object sender, EventArgs e)
		{
			if (!this.playing)
			{
				this.playing = true;
				this.cancellationTokenSource = new CancellationTokenSource();

				await Task.Run(async () =>
				{
					this.SetPlayButtonText("Stop");

					while (true)
					{
						int currentSongIndex = this.dataGridView1.CurrentRow.Index;
						this.dataGridView1.Rows[currentSongIndex].DefaultCellStyle.BackColor = Color.LightGray;

						await this.midiFileReader.PlaySong(this.CurrentDirectory, this.SelectedSong, this.cancellationTokenSource.Token);

						this.dataGridView1.Rows[currentSongIndex].DefaultCellStyle.BackColor = Color.Empty;

						if (this.cancellationTokenSource.Token.IsCancellationRequested || this.StopAfterCurrent)
						{
							break;
						}

						int nextIndex = -1;
						if (this.dataGridView1.CurrentRow.Index != currentSongIndex && this.PlaybackFollowsCursor)
						{
							// play next highlighted song, don't move cursor
							nextIndex = this.dataGridView1.CurrentRow.Index;
						}
						else
						{
							nextIndex = this.getNextPlaybackIndex(currentSongIndex);
						}

						if (nextIndex >= 0)
						{
							this.Invoke(new Action(() =>
							{
								this.dataGridView1.CurrentCell = this.dataGridView1[0, nextIndex];
							}));
							await Task.Delay(1000); // temporary, delay before next song
						}
						else
						{
							break;
						}
					}

					this.SetPlayButtonText("Play");
					this.playing = false;

				}, this.cancellationTokenSource.Token);
			}
			else
			{
				this.cancellationTokenSource.Cancel();
				this.SetPlayButtonText("Play");
				this.playing = false;
			}
		}

		private int getNextPlaybackIndex(int previousSongIndex)
		{
			int currentIndex = previousSongIndex;

			switch (this.Playback)
			{
				case Playback.Default:
					currentIndex++;
					if (currentIndex >= this.dataGridView1.Rows.Count) currentIndex = -1;
					break;

				case Playback.RepeatPlaylist:
					currentIndex++;
					if (currentIndex >= this.dataGridView1.Rows.Count) currentIndex = 0;
					break;

				case Playback.Shuffle:
					currentIndex = new Random().Next(0, this.dataGridView1.Rows.Count);
					break;

				case Playback.RepeatTrack:
				default:
					break;
			}

			return currentIndex;
		}

		private static async Task ExitPerformance()
		{
			await Task.Delay(1000);
			await GameInput.SendPress(Keys.Escape, handleIndex: -1);
		}

		private static async Task Emote(string emote)
		{
			await Task.Delay(1500);
			await GameInput.SendText(emote);
		}

		private void MidiFileReader_NoteReceived(int note, int players, bool on)
		{
			this.notePlayer.PlayNote(note, players, on);
		}

		private void MidiFileReader_Progress(decimal progress)
		{
			this.BeginInvoke(new Action(() =>
			{
				int value = (int)(this.trackBar1.Maximum * progress);
				value = Math.Max(this.trackBar1.Minimum, Math.Min(value, this.trackBar1.Maximum)); // clamp
				this.trackBar1.Value = value;
				TimeSpan currentTime = new TimeSpan(0, 0, 0, 0, (int)(progress * this.songTotalTimeMs));
				TimeSpan totalTime = new TimeSpan(0, 0, 0, 0, (int)this.songTotalTimeMs);
				this.label_ProgressTime.Text = $"{currentTime:mm\\:ss} / {totalTime:mm\\:ss}";
			}));
		}

		private void MidiFileReader_TotalTime(decimal totalTime)
		{
			this.songTotalTimeMs = totalTime;
		}

		private void SetPlayButtonText(string text)
		{
			this.SafeInvoke(() => this.button_Play.Text = text);
		}

		private void RefreshFileThings()
		{
			try
			{
				this.songDataSource.Clear();
				// set up datatable columns, this is dumb do it better
				new List<SongArrangment>() { new SongArrangment() }.ToDataTable(ref this.songDataSource);
				this.songDataSource.Rows[0].Delete();

				this.LoadSongArrangments();
			}
			catch { }

			if (this.TryGetMidiDirectoryInfo(out DirectoryInfo midiDirectoryInfo))
			{
				foreach (string name in midiDirectoryInfo.GetFiles("*.mid*").Select(fi => fi.Name))
				{
					if (false == this.songDataSource.AsEnumerable().Any(row => String.Equals(row.Field<string>("Name"), name)))
					{
						DataRow row = this.songDataSource.NewRow();
						row["Name"] = name;
						row["SemitoneShift"] = 0;
						row["EnsembleMatrix"] = Enumerable.Range(0, 16).Select(i => 1).ToArray();
						this.songDataSource.Rows.Add(row);
					}
				}

				Properties.Settings.Default.MidiDirectory = this.textBox_MidiDirectory.Text;
				Properties.Settings.Default.Save();
			}
			this.RefreshSelectedFile();
		}

		private bool TryGetMidiDirectoryInfo(out DirectoryInfo midiDirectoryInfo)
		{
			midiDirectoryInfo = null;
			string midiDirectoryPath = this.textBox_MidiDirectory.Text;

			if (String.IsNullOrWhiteSpace(midiDirectoryPath)) return false;

			midiDirectoryInfo = new DirectoryInfo(midiDirectoryPath);
			return midiDirectoryInfo.Exists;
		}

		private void RefreshSelectedFile()
		{
			string selectedMidiFile = null;

			if (this.dataGridView1.SelectedRows.Count > 0)
			{
				selectedMidiFile = this.SelectedSong.GetSongFilePath(this.CurrentDirectory);

				this.notePlayer.SemitoneShift = this.SelectedSong.SemitoneShift;
				this.numericUpDown_SemitoneShift.Value = this.SelectedSong.SemitoneShift;
				this.checkBox_ArpeggiateUp.Checked = this.SelectedSong.ArpeggiateUp;
			}

			this.button_Play.Enabled = true
				&& !String.IsNullOrWhiteSpace(selectedMidiFile)
				&& File.Exists(selectedMidiFile)
			;
		}

		private void SafeInvoke(Action a)
		{
			if (this.InvokeRequired) this.Invoke(a);
			else a?.Invoke();
		}

		private void button_Browse_Click(object sender, EventArgs e)
		{
			FolderBrowserDialog fbd = new FolderBrowserDialog();

			if (this.TryGetMidiDirectoryInfo(out DirectoryInfo midiDirectoryInfo))
			{
				fbd.SelectedPath = midiDirectoryInfo.FullName;
			}

			if (fbd.ShowDialog() == DialogResult.OK)
			{
				this.textBox_MidiDirectory.Text = fbd.SelectedPath;
			}
		}

		private void textBox_MidiDirectory_TextChanged(object sender, EventArgs e)
		{
			this.RefreshFileThings();
		}

		private void dataGridView1_CellEndEdit(object sender, DataGridViewCellEventArgs e)
		{
			this.SaveSongArrangements();
			this.RefreshSelectedFile();
		}

		private void dataGridView1_SelectionChanged(object sender, EventArgs e)
		{
			this.RefreshSelectedFile();
		}

		private void SaveSongArrangements()
		{
			if (this.TryGetArrangementsPath(out string path))
			{
				this.songDataSource.WriteXml(path);
			}
		}

		private List<SongArrangment> LoadSongArrangments()
		{
			if (this.TryGetArrangementsPath(out string path))
			{
				this.songDataSource.ReadXml(path);
				return new List<SongArrangment>();
			}
			else
			{
				return new List<SongArrangment>();
			}
		}

		private bool TryGetArrangementsPath(out string path)
		{
			if (this.TryGetMidiDirectoryInfo(out DirectoryInfo midiDirectoryInfo))
			{
				path = Path.Combine(
					midiDirectoryInfo.FullName,
					"arrangements.xml"
				);
				return true;
			}
			else
			{
				path = null;
				return false;
			}
		}

		private void button_Refresh_Click(object sender, EventArgs e)
		{
			this.RefreshFileThings();
		}

		private void button_Ident_Click(object sender, EventArgs e)
		{
			try
			{
				Regex regex = new Regex(@"( <\d+>)+$");

				for (int i = 0; i < GameInput.GameHandles.Length; i++)
				{
					IntPtr handle = GameInput.GameHandles[i];
					StringBuilder sb = new StringBuilder();
					NativeMethods.GetWindowText(handle, sb, 256);
					string title = sb.ToString();
					title = regex.Replace(title, String.Empty);
					NativeMethods.SetWindowText(handle, $"{title} <{i + 1}>");
				}
			}
			catch (AccessViolationException) { /* i don't know */ }
		}

		private void comboBox_GameProcesses_SelectedIndexChanged(object sender, EventArgs e)
		{
			//this.comboBox_GameProcesses.SelectedIndex;
		}

		private void exitToolStripMenuItem_Click(object sender, EventArgs e)
		{
			this.Close();
		}

		private void alwaysOnTopToolStripMenuItem_Click(object sender, EventArgs e)
		{
			this.alwaysOnTopToolStripMenuItem.Checked = !this.alwaysOnTopToolStripMenuItem.Checked;
			this.TopMost = this.alwaysOnTopToolStripMenuItem.Checked;
		}

		private void comboBoxMidiInDevices_SelectedIndexChanged(object sender, EventArgs e)
		{
			this.button_Monitor.Enabled = this.comboBoxMidiInDevices.SelectedIndex >= 0;
		}

		private void button1_Click(object sender, EventArgs e)
		{
			this.textBox_Filter.Text = String.Empty;
		}

		private void textBox_Filter_TextChanged(object sender, EventArgs e)
		{
			string whereClause = $"Name LIKE '%{this.textBox_Filter.Text}%'";
			(this.dataGridView1.DataSource as DataTable).DefaultView.RowFilter = whereClause;
		}

		private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
		{
			DataGridView grid = (DataGridView)sender;

			if (e.RowIndex < 0)
			{
				// User clicked the header column, do nothing
				return;
			}

			if (grid[e.ColumnIndex, e.RowIndex] is DataGridViewButtonCell)
			{
				Action<DataGridViewRow> clickHandler = (Action<DataGridViewRow>)grid.Columns[e.ColumnIndex].Tag;
				DataGridViewRow row = grid.Rows[e.RowIndex];
				object o = row.DataBoundItem;

				clickHandler(row);
			}
		}

		private void EnsembleClickHandler(DataGridViewRow row)
		{
			EnsembleForm ensembleForm = new EnsembleForm(this.SelectedSong, this.CurrentDirectory);
			ensembleForm.SongUpdated += (song) =>
			{
				this.songDataSource.Rows[row.Index].SetField("EnsembleMatrix", song.EnsembleMatrix);
				this.SaveSongArrangements();
			};
			ensembleForm.Show(this);
		}

		private void dataGridView1_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
		{
			DataGridView grid = (DataGridView)sender;
			if (grid.Columns[e.ColumnIndex].DataPropertyName == "Name" && e.RowIndex >= 0)
			{
				e.Value = Path.GetFileNameWithoutExtension(e.Value.ToString());
				e.FormattingApplied = true;
			}

			//if (e.RowIndex == this.currentSongIndex && e.RowIndex >= 0 && e.RowIndex < grid.Rows.Count)
			//{
			//	//grid.Rows[e.RowIndex].DefaultCellStyle.BackColor = Color.LightGray;
			//	e.CellStyle.BackColor = Color.LightGray;
			//	e.FormattingApplied = true;
			//}
		}

		private void checkBox_AutoOctave_CheckedChanged(object sender, EventArgs e)
		{
			this.notePlayer.AutoOctaveShift = this.checkBox_AutoOctave.Checked;
		}

		private void checkBox_ArpeggiateUp_CheckedChanged(object sender, EventArgs e)
		{
			this.notePlayer.ArpeggiateUp = this.checkBox_ArpeggiateUp.Checked;
		}

		private async void aFKModeToolStripMenuItem_Click(object sender, EventArgs e)
		{
			this.aFKModeToolStripMenuItem.Checked = !this.aFKModeToolStripMenuItem.Checked;

			if (this.aFKModeToolStripMenuItem.Checked)
			{
				afkCancellationTokenSource = new CancellationTokenSource();

				await Task.Run(async () =>
				{
					GameInput.RefreshGameHandles();

					while (false == afkCancellationTokenSource.IsCancellationRequested)
					{
						for (int i = 0; i < GameInput.GameHandles.Length; i++)
						{
							if (Enum.TryParse(this.AfkKeybind, out Keys key))
							{
								await GameInput.SendPress(key, handleIndex: i);
							}
						}
						//await GameInput.SendPress(Keys.F);
						await Task.Delay(60000);
					}
				}, afkCancellationTokenSource.Token);
			}
			else
			{
				afkCancellationTokenSource?.Cancel();
			}
		}

		private void toolStripTextBox1_TextChanged(object sender, EventArgs e)
		{
			this.AfkKeybind = this.AfkKeybind.ToUpper();

			if (String.IsNullOrEmpty(this.AfkKeybind) || this.AfkKeybind.Length > 1)
			{
				this.AfkKeybind = this.AfkKeybind.Substring(0, 1);
			}

			if (Enum.TryParse(this.AfkKeybind, out Keys _))
			{
				Properties.Settings.Default.AfkKeybind = this.AfkKeybind;
				Properties.Settings.Default.Save();
			}
		}

		private void stopAfterCurrentToolStripMenuItem_Click(object sender, EventArgs e)
		{
			this.StopAfterCurrent = !this.StopAfterCurrent;
			Properties.Settings.Default.StopAfterCurrent = this.StopAfterCurrent;
			Properties.Settings.Default.Save();
		}

		private void playbackFollowsCursorToolStripMenuItem_Click(object sender, EventArgs e)
		{
			this.PlaybackFollowsCursor = !this.PlaybackFollowsCursor;
			Properties.Settings.Default.PlaybackFollowsCursor = this.PlaybackFollowsCursor;
			Properties.Settings.Default.Save();
		}

		private void DropDown_Closing(object sender, ToolStripDropDownClosingEventArgs e)
		{
			if (e.CloseReason == ToolStripDropDownCloseReason.ItemClicked)
			{
				e.Cancel = true;
			}
		}

		private void orderToolStripMenuItem_DropDownItemClicked(object sender, ToolStripItemClickedEventArgs e)
		{
			ToolStripItem item = e.ClickedItem;
			this.Playback = (Playback)item.Tag;

			Properties.Settings.Default.PlaybackMode = (int)this.Playback;
			Properties.Settings.Default.Save();

			this.UpdatePlaybackOrderMenuItems();
		}

		private void UpdatePlaybackOrderMenuItems()
		{
			foreach (ToolStripMenuItem subItem in this.orderToolStripMenuItem.DropDownItems)
			{
				//subItem.Checked = BitwiseExtensions.IsEnabled((int)this.Playback, (int)subItem.Tag);
				subItem.Checked = this.Playback == (Playback)subItem.Tag;
			}
		}
	}
}
