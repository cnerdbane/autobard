﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutoBard
{
	public class SongArrangment
	{
		public string Name { get; set; }
		public string Notes { get; set; }
		public int SemitoneShift { get; set; }
		public bool ArpeggiateUp { get; set; }
		public int[] EnsembleMatrix { get; set; }


		public string GetSongFilePath(string directory)
		{
			return Path.Combine(
				directory,
				this.Name
			);
		}
	}
}
