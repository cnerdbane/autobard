﻿using AutoBard.Extensions;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AutoBard
{
	public partial class EnsembleForm : Form
	{
		private readonly SongArrangment song;
		private readonly string directory;
		private string[] trackNames;

		public delegate void SongUpdatedEvent(SongArrangment song);
		public event SongUpdatedEvent SongUpdated;

		public EnsembleForm(SongArrangment song, string directory)
		{
			InitializeComponent();
			this.song = song;
			this.directory = directory;
		}

		private async void EnsembleForm_Shown(object sender, EventArgs e)
		{
			this.label_SongName.Text = this.song.Name;
			await InitializeTable();
		}

		private async Task InitializeTable()
		{
			await Task.Run(() =>
			{
				MidiFileReader mfr = new MidiFileReader();
				this.trackNames = mfr.GetTrackNames(directory, this.song);
			});

			if (this.song.EnsembleMatrix is null)
			{
				this.song.EnsembleMatrix = new int[trackNames.Length];
			}

			for (int i = 0; i < this.song.EnsembleMatrix.Length; i++)
			{
				int colIndex = this.dataGridView1.Columns.Add("Player", (i + 1).ToString());
				DataGridViewColumn newCol = this.dataGridView1.Columns[colIndex];
				newCol.AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCellsExceptHeader;
			}

			// add rows for each channel
			for (int i = 0; i < this.trackNames.Length; i++)
			{
				int newIndex = this.dataGridView1.Rows.Add();
				this.dataGridView1.Rows[newIndex].HeaderCell.Value = $"{i + 1}: {this.trackNames[i]}";
			}
		}

		private void dataGridView1_SelectionChanged(object sender, EventArgs e)
		{
			this.dataGridView1.ClearSelection();
		}

		private void button_Cancel_Click(object sender, EventArgs e)
		{
			this.Close();
		}

		private void button_Save_Click(object sender, EventArgs e)
		{
			this.SongUpdated(this.song);
			this.Close();
		}

		private void dataGridView1_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
		{
			if (e.RowIndex >= 0)
			{
				int channelIndex = e.RowIndex;
				int playerIndex = e.ColumnIndex;

				e.Value = String.Empty;
				e.CellStyle.BackColor = this.GetChannelEnabledForPlayer(playerIndex, channelIndex) ? Color.LimeGreen : Color.Red;
				e.FormattingApplied = true;
			}
		}

		private bool GetChannelEnabledForPlayer(int player, int channel)
		{
			return BitwiseExtensions.IsEnabled(this.song.EnsembleMatrix[channel], player);
		}

		private void ToggleChannelEnabledForPlayer(int player, int channel)
		{
			this.song.EnsembleMatrix[channel] = BitwiseExtensions.Toggle(this.song.EnsembleMatrix[channel], player);
		}

		private void SetChannelEnabledForPlayer(int player, int channel, bool enabled)
		{
			if (enabled)
			{
				this.song.EnsembleMatrix[channel] = BitwiseExtensions.Enable(this.song.EnsembleMatrix[channel], player);
			}
			else
			{
				this.song.EnsembleMatrix[channel] = BitwiseExtensions.Disable(this.song.EnsembleMatrix[channel], player);
			}
		}

		private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
		{
			if (e.RowIndex < 0 && e.ColumnIndex < 0)
			{
				// User clicked the header corner thingy, do nothing
				return;
			}

			int channelIndex = e.RowIndex;
			int playerIndex = e.ColumnIndex;

			if (e.ColumnIndex < 0)
			{
				// User clicked row header, toggle channel for all players
				bool enable = !this.GetChannelEnabledForPlayer(0, channelIndex);
				for (int i = 0; i < this.song.EnsembleMatrix.Length; i++)
				{
					this.SetChannelEnabledForPlayer(i, channelIndex, enable);
				}
			}
			else if (e.RowIndex < 0)
			{
				// User clicked column header, toggle player for all channels
				bool enable = !this.GetChannelEnabledForPlayer(playerIndex, 0);
				for (int i = 0; i < this.trackNames.Length; i++)
				{
					this.SetChannelEnabledForPlayer(playerIndex, i, enable);
				}
			}
			else
			{
				// User clicked single player and single track
				this.ToggleChannelEnabledForPlayer(playerIndex, channelIndex);
			}

			// repaint
			this.dataGridView1.Invalidate();
		}

		private void button_ResetDefault_Click(object sender, EventArgs e)
		{
			this.song.EnsembleMatrix = Enumerable.Range(0, 16).Select(i => 1).ToArray();

			// repaint
			this.dataGridView1.Invalidate();
		}
	}
}
