﻿using System.Windows.Forms;

namespace AutoBard
{
	partial class AutoBardForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AutoBardForm));
			this.comboBoxMidiInDevices = new System.Windows.Forms.ComboBox();
			this.button_Monitor = new System.Windows.Forms.Button();
			this.checkBox_ArpeggiateUp = new System.Windows.Forms.CheckBox();
			this.numericUpDown_SemitoneShift = new System.Windows.Forms.NumericUpDown();
			this.textBox_MidiDirectory = new System.Windows.Forms.TextBox();
			this.button_Play = new System.Windows.Forms.Button();
			this.button_Browse = new System.Windows.Forms.Button();
			this.label1 = new System.Windows.Forms.Label();
			this.dataGridView1 = new System.Windows.Forms.DataGridView();
			this.nameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.Notes = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.semitoneShiftDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.arpeggiateUpDataGridViewCheckBoxColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
			this.Ensemble = new System.Windows.Forms.DataGridViewButtonColumn();
			this.button_Refresh = new System.Windows.Forms.Button();
			this.comboBox_GameProcesses = new System.Windows.Forms.ComboBox();
			this.button_Ident = new System.Windows.Forms.Button();
			this.label2 = new System.Windows.Forms.Label();
			this.panel1 = new System.Windows.Forms.Panel();
			this.panel2 = new System.Windows.Forms.Panel();
			this.label_ProgressTime = new System.Windows.Forms.Label();
			this.trackBar1 = new System.Windows.Forms.TrackBar();
			this.label3 = new System.Windows.Forms.Label();
			this.button1 = new System.Windows.Forms.Button();
			this.textBox_Filter = new System.Windows.Forms.TextBox();
			this.menuStrip1 = new System.Windows.Forms.MenuStrip();
			this.menuToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.alwaysOnTopToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.toolsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.aFKModeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.toolStripTextBox1 = new System.Windows.Forms.ToolStripTextBox();
			this.playbackToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.orderToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
			this.stopAfterCurrentToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.playbackFollowsCursorToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.tabControl1 = new System.Windows.Forms.TabControl();
			this.tabPage2 = new System.Windows.Forms.TabPage();
			this.checkBox_AutoOctave = new System.Windows.Forms.CheckBox();
			this.panel_ProcessSelector = new System.Windows.Forms.Panel();
			this.label4 = new System.Windows.Forms.Label();
			this.tabPage1 = new System.Windows.Forms.TabPage();
			this.defaultToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.repeatPlaylistToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.repeatTrackToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.shuffleToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			((System.ComponentModel.ISupportInitialize)(this.numericUpDown_SemitoneShift)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
			this.panel1.SuspendLayout();
			this.panel2.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.trackBar1)).BeginInit();
			this.menuStrip1.SuspendLayout();
			this.tabControl1.SuspendLayout();
			this.tabPage2.SuspendLayout();
			this.panel_ProcessSelector.SuspendLayout();
			this.tabPage1.SuspendLayout();
			this.SuspendLayout();
			// 
			// comboBoxMidiInDevices
			// 
			this.comboBoxMidiInDevices.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.comboBoxMidiInDevices.FormattingEnabled = true;
			this.comboBoxMidiInDevices.Location = new System.Drawing.Point(0, 22);
			this.comboBoxMidiInDevices.Margin = new System.Windows.Forms.Padding(18, 16, 18, 16);
			this.comboBoxMidiInDevices.Name = "comboBoxMidiInDevices";
			this.comboBoxMidiInDevices.Size = new System.Drawing.Size(898, 34);
			this.comboBoxMidiInDevices.TabIndex = 0;
			this.comboBoxMidiInDevices.SelectedIndexChanged += new System.EventHandler(this.comboBoxMidiInDevices_SelectedIndexChanged);
			// 
			// button_Monitor
			// 
			this.button_Monitor.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.button_Monitor.Location = new System.Drawing.Point(938, 22);
			this.button_Monitor.Margin = new System.Windows.Forms.Padding(18, 16, 18, 16);
			this.button_Monitor.Name = "button_Monitor";
			this.button_Monitor.Size = new System.Drawing.Size(186, 42);
			this.button_Monitor.TabIndex = 1;
			this.button_Monitor.Text = "Monitor";
			this.button_Monitor.UseVisualStyleBackColor = true;
			this.button_Monitor.Click += new System.EventHandler(this.button_Monitor_Click);
			// 
			// checkBox_ArpeggiateUp
			// 
			this.checkBox_ArpeggiateUp.AutoSize = true;
			this.checkBox_ArpeggiateUp.Checked = true;
			this.checkBox_ArpeggiateUp.CheckState = System.Windows.Forms.CheckState.Checked;
			this.checkBox_ArpeggiateUp.Location = new System.Drawing.Point(24, 168);
			this.checkBox_ArpeggiateUp.Margin = new System.Windows.Forms.Padding(18, 16, 18, 16);
			this.checkBox_ArpeggiateUp.Name = "checkBox_ArpeggiateUp";
			this.checkBox_ArpeggiateUp.Size = new System.Drawing.Size(183, 30);
			this.checkBox_ArpeggiateUp.TabIndex = 2;
			this.checkBox_ArpeggiateUp.Text = "Arpeggiate Up";
			this.checkBox_ArpeggiateUp.UseVisualStyleBackColor = true;
			this.checkBox_ArpeggiateUp.CheckedChanged += new System.EventHandler(this.checkBox_ArpeggiateUp_CheckedChanged);
			// 
			// numericUpDown_SemitoneShift
			// 
			this.numericUpDown_SemitoneShift.Location = new System.Drawing.Point(24, 96);
			this.numericUpDown_SemitoneShift.Margin = new System.Windows.Forms.Padding(18, 16, 18, 16);
			this.numericUpDown_SemitoneShift.Maximum = new decimal(new int[] {
            48,
            0,
            0,
            0});
			this.numericUpDown_SemitoneShift.Minimum = new decimal(new int[] {
            48,
            0,
            0,
            -2147483648});
			this.numericUpDown_SemitoneShift.Name = "numericUpDown_SemitoneShift";
			this.numericUpDown_SemitoneShift.Size = new System.Drawing.Size(108, 32);
			this.numericUpDown_SemitoneShift.TabIndex = 3;
			// 
			// textBox_MidiDirectory
			// 
			this.textBox_MidiDirectory.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.textBox_MidiDirectory.Location = new System.Drawing.Point(128, 14);
			this.textBox_MidiDirectory.Margin = new System.Windows.Forms.Padding(18, 16, 18, 16);
			this.textBox_MidiDirectory.Name = "textBox_MidiDirectory";
			this.textBox_MidiDirectory.Size = new System.Drawing.Size(746, 32);
			this.textBox_MidiDirectory.TabIndex = 4;
			// 
			// button_Play
			// 
			this.button_Play.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.button_Play.Location = new System.Drawing.Point(1008, 477);
			this.button_Play.Margin = new System.Windows.Forms.Padding(18, 16, 18, 16);
			this.button_Play.Name = "button_Play";
			this.button_Play.Size = new System.Drawing.Size(128, 42);
			this.button_Play.TabIndex = 5;
			this.button_Play.Text = "Play";
			this.button_Play.UseVisualStyleBackColor = true;
			this.button_Play.Click += new System.EventHandler(this.button_Play_Click);
			// 
			// button_Browse
			// 
			this.button_Browse.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.button_Browse.Location = new System.Drawing.Point(914, 12);
			this.button_Browse.Margin = new System.Windows.Forms.Padding(18, 16, 18, 16);
			this.button_Browse.Name = "button_Browse";
			this.button_Browse.Size = new System.Drawing.Size(58, 42);
			this.button_Browse.TabIndex = 6;
			this.button_Browse.Text = "...";
			this.button_Browse.UseVisualStyleBackColor = true;
			this.button_Browse.Click += new System.EventHandler(this.button_Browse_Click);
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(150, 104);
			this.label1.Margin = new System.Windows.Forms.Padding(18, 0, 18, 0);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(155, 26);
			this.label1.TabIndex = 9;
			this.label1.Text = "Semitone Shift";
			// 
			// dataGridView1
			// 
			this.dataGridView1.AllowUserToAddRows = false;
			this.dataGridView1.AllowUserToDeleteRows = false;
			this.dataGridView1.AllowUserToOrderColumns = true;
			this.dataGridView1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.nameDataGridViewTextBoxColumn,
            this.Notes,
            this.semitoneShiftDataGridViewTextBoxColumn,
            this.arpeggiateUpDataGridViewCheckBoxColumn,
            this.Ensemble});
			this.dataGridView1.Location = new System.Drawing.Point(0, 58);
			this.dataGridView1.Margin = new System.Windows.Forms.Padding(26, 24, 26, 24);
			this.dataGridView1.MultiSelect = false;
			this.dataGridView1.Name = "dataGridView1";
			this.dataGridView1.RowHeadersVisible = false;
			this.dataGridView1.RowHeadersWidth = 82;
			this.dataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
			this.dataGridView1.Size = new System.Drawing.Size(1136, 379);
			this.dataGridView1.TabIndex = 13;
			this.dataGridView1.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellContentClick);
			this.dataGridView1.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellEndEdit);
			this.dataGridView1.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.dataGridView1_CellFormatting);
			// 
			// nameDataGridViewTextBoxColumn
			// 
			this.nameDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
			this.nameDataGridViewTextBoxColumn.DataPropertyName = "Name";
			this.nameDataGridViewTextBoxColumn.HeaderText = "Name";
			this.nameDataGridViewTextBoxColumn.MinimumWidth = 10;
			this.nameDataGridViewTextBoxColumn.Name = "nameDataGridViewTextBoxColumn";
			this.nameDataGridViewTextBoxColumn.ReadOnly = true;
			// 
			// Notes
			// 
			this.Notes.DataPropertyName = "Notes";
			this.Notes.HeaderText = "Notes";
			this.Notes.MinimumWidth = 10;
			this.Notes.Name = "Notes";
			this.Notes.Width = 200;
			// 
			// semitoneShiftDataGridViewTextBoxColumn
			// 
			this.semitoneShiftDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCellsExceptHeader;
			this.semitoneShiftDataGridViewTextBoxColumn.DataPropertyName = "SemitoneShift";
			this.semitoneShiftDataGridViewTextBoxColumn.HeaderText = "S";
			this.semitoneShiftDataGridViewTextBoxColumn.MinimumWidth = 10;
			this.semitoneShiftDataGridViewTextBoxColumn.Name = "semitoneShiftDataGridViewTextBoxColumn";
			this.semitoneShiftDataGridViewTextBoxColumn.ToolTipText = "Semitone Shift";
			this.semitoneShiftDataGridViewTextBoxColumn.Width = 10;
			// 
			// arpeggiateUpDataGridViewCheckBoxColumn
			// 
			this.arpeggiateUpDataGridViewCheckBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCellsExceptHeader;
			this.arpeggiateUpDataGridViewCheckBoxColumn.DataPropertyName = "ArpeggiateUp";
			this.arpeggiateUpDataGridViewCheckBoxColumn.HeaderText = "A";
			this.arpeggiateUpDataGridViewCheckBoxColumn.MinimumWidth = 10;
			this.arpeggiateUpDataGridViewCheckBoxColumn.Name = "arpeggiateUpDataGridViewCheckBoxColumn";
			this.arpeggiateUpDataGridViewCheckBoxColumn.ToolTipText = "Arpeggiate Up";
			this.arpeggiateUpDataGridViewCheckBoxColumn.Width = 10;
			// 
			// Ensemble
			// 
			this.Ensemble.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCellsExceptHeader;
			this.Ensemble.HeaderText = "E";
			this.Ensemble.MinimumWidth = 10;
			this.Ensemble.Name = "Ensemble";
			this.Ensemble.Resizable = System.Windows.Forms.DataGridViewTriState.True;
			this.Ensemble.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
			this.Ensemble.Text = ">";
			this.Ensemble.ToolTipText = "Open Ensemble Picker";
			this.Ensemble.UseColumnTextForButtonValue = true;
			this.Ensemble.Width = 10;
			// 
			// button_Refresh
			// 
			this.button_Refresh.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.button_Refresh.Location = new System.Drawing.Point(1008, 12);
			this.button_Refresh.Margin = new System.Windows.Forms.Padding(18, 16, 18, 16);
			this.button_Refresh.Name = "button_Refresh";
			this.button_Refresh.Size = new System.Drawing.Size(128, 42);
			this.button_Refresh.TabIndex = 15;
			this.button_Refresh.Text = "Refresh";
			this.button_Refresh.UseVisualStyleBackColor = true;
			this.button_Refresh.Click += new System.EventHandler(this.button_Refresh_Click);
			// 
			// comboBox_GameProcesses
			// 
			this.comboBox_GameProcesses.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.comboBox_GameProcesses.FormattingEnabled = true;
			this.comboBox_GameProcesses.Location = new System.Drawing.Point(122, 12);
			this.comboBox_GameProcesses.Margin = new System.Windows.Forms.Padding(14, 12, 14, 12);
			this.comboBox_GameProcesses.Name = "comboBox_GameProcesses";
			this.comboBox_GameProcesses.Size = new System.Drawing.Size(54, 34);
			this.comboBox_GameProcesses.TabIndex = 17;
			this.comboBox_GameProcesses.SelectedIndexChanged += new System.EventHandler(this.comboBox_GameProcesses_SelectedIndexChanged);
			// 
			// button_Ident
			// 
			this.button_Ident.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.button_Ident.Location = new System.Drawing.Point(216, 10);
			this.button_Ident.Margin = new System.Windows.Forms.Padding(18, 16, 18, 16);
			this.button_Ident.Name = "button_Ident";
			this.button_Ident.Size = new System.Drawing.Size(128, 42);
			this.button_Ident.TabIndex = 18;
			this.button_Ident.Text = "Ident";
			this.button_Ident.UseVisualStyleBackColor = true;
			this.button_Ident.Click += new System.EventHandler(this.button_Ident_Click);
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(-6, 20);
			this.label2.Margin = new System.Windows.Forms.Padding(14, 0, 14, 0);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(103, 26);
			this.label2.TabIndex = 19;
			this.label2.Text = "Midi Path";
			// 
			// panel1
			// 
			this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.panel1.Controls.Add(this.label2);
			this.panel1.Controls.Add(this.textBox_MidiDirectory);
			this.panel1.Controls.Add(this.button_Browse);
			this.panel1.Controls.Add(this.button_Refresh);
			this.panel1.Location = new System.Drawing.Point(6, 84);
			this.panel1.Margin = new System.Windows.Forms.Padding(14, 12, 14, 12);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(1136, 70);
			this.panel1.TabIndex = 20;
			// 
			// panel2
			// 
			this.panel2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.panel2.Controls.Add(this.label_ProgressTime);
			this.panel2.Controls.Add(this.trackBar1);
			this.panel2.Controls.Add(this.label3);
			this.panel2.Controls.Add(this.button1);
			this.panel2.Controls.Add(this.textBox_Filter);
			this.panel2.Controls.Add(this.dataGridView1);
			this.panel2.Controls.Add(this.button_Play);
			this.panel2.Location = new System.Drawing.Point(6, 166);
			this.panel2.Margin = new System.Windows.Forms.Padding(0);
			this.panel2.Name = "panel2";
			this.panel2.Size = new System.Drawing.Size(1136, 535);
			this.panel2.TabIndex = 21;
			// 
			// label_ProgressTime
			// 
			this.label_ProgressTime.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.label_ProgressTime.AutoSize = true;
			this.label_ProgressTime.Location = new System.Drawing.Point(3, 494);
			this.label_ProgressTime.Name = "label_ProgressTime";
			this.label_ProgressTime.Size = new System.Drawing.Size(138, 26);
			this.label_ProgressTime.TabIndex = 22;
			this.label_ProgressTime.Text = "00:00 / 00:00";
			// 
			// trackBar1
			// 
			this.trackBar1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.trackBar1.Location = new System.Drawing.Point(147, 477);
			this.trackBar1.Maximum = 100;
			this.trackBar1.Name = "trackBar1";
			this.trackBar1.Size = new System.Drawing.Size(840, 90);
			this.trackBar1.TabIndex = 21;
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Location = new System.Drawing.Point(-2, 8);
			this.label3.Margin = new System.Windows.Forms.Padding(14, 0, 14, 0);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(81, 26);
			this.label3.TabIndex = 20;
			this.label3.Text = "Search";
			// 
			// button1
			// 
			this.button1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.button1.Location = new System.Drawing.Point(1094, 0);
			this.button1.Margin = new System.Windows.Forms.Padding(14, 12, 14, 12);
			this.button1.Name = "button1";
			this.button1.Size = new System.Drawing.Size(42, 42);
			this.button1.TabIndex = 15;
			this.button1.Text = "X";
			this.button1.UseVisualStyleBackColor = true;
			this.button1.Click += new System.EventHandler(this.button1_Click);
			// 
			// textBox_Filter
			// 
			this.textBox_Filter.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.textBox_Filter.Location = new System.Drawing.Point(128, 0);
			this.textBox_Filter.Margin = new System.Windows.Forms.Padding(14, 12, 14, 12);
			this.textBox_Filter.Name = "textBox_Filter";
			this.textBox_Filter.Size = new System.Drawing.Size(934, 32);
			this.textBox_Filter.TabIndex = 14;
			this.textBox_Filter.TextChanged += new System.EventHandler(this.textBox_Filter_TextChanged);
			// 
			// menuStrip1
			// 
			this.menuStrip1.GripMargin = new System.Windows.Forms.Padding(2, 2, 0, 2);
			this.menuStrip1.ImageScalingSize = new System.Drawing.Size(32, 32);
			this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuToolStripMenuItem,
            this.toolsToolStripMenuItem,
            this.playbackToolStripMenuItem});
			this.menuStrip1.Location = new System.Drawing.Point(0, 0);
			this.menuStrip1.Name = "menuStrip1";
			this.menuStrip1.Size = new System.Drawing.Size(1164, 48);
			this.menuStrip1.TabIndex = 22;
			this.menuStrip1.Text = "menuStrip1";
			// 
			// menuToolStripMenuItem
			// 
			this.menuToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.alwaysOnTopToolStripMenuItem,
            this.exitToolStripMenuItem});
			this.menuToolStripMenuItem.Name = "menuToolStripMenuItem";
			this.menuToolStripMenuItem.Size = new System.Drawing.Size(98, 44);
			this.menuToolStripMenuItem.Text = "Menu";
			// 
			// alwaysOnTopToolStripMenuItem
			// 
			this.alwaysOnTopToolStripMenuItem.Name = "alwaysOnTopToolStripMenuItem";
			this.alwaysOnTopToolStripMenuItem.Size = new System.Drawing.Size(299, 44);
			this.alwaysOnTopToolStripMenuItem.Text = "Always on top";
			this.alwaysOnTopToolStripMenuItem.Click += new System.EventHandler(this.alwaysOnTopToolStripMenuItem_Click);
			// 
			// exitToolStripMenuItem
			// 
			this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
			this.exitToolStripMenuItem.Size = new System.Drawing.Size(299, 44);
			this.exitToolStripMenuItem.Text = "Exit";
			this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
			// 
			// toolsToolStripMenuItem
			// 
			this.toolsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.aFKModeToolStripMenuItem});
			this.toolsToolStripMenuItem.Name = "toolsToolStripMenuItem";
			this.toolsToolStripMenuItem.Size = new System.Drawing.Size(90, 44);
			this.toolsToolStripMenuItem.Text = "Tools";
			// 
			// aFKModeToolStripMenuItem
			// 
			this.aFKModeToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripTextBox1});
			this.aFKModeToolStripMenuItem.Name = "aFKModeToolStripMenuItem";
			this.aFKModeToolStripMenuItem.Size = new System.Drawing.Size(260, 44);
			this.aFKModeToolStripMenuItem.Text = "AFK Mode";
			this.aFKModeToolStripMenuItem.Click += new System.EventHandler(this.aFKModeToolStripMenuItem_Click);
			// 
			// toolStripTextBox1
			// 
			this.toolStripTextBox1.Font = new System.Drawing.Font("Segoe UI", 9F);
			this.toolStripTextBox1.Name = "toolStripTextBox1";
			this.toolStripTextBox1.Size = new System.Drawing.Size(100, 39);
			this.toolStripTextBox1.Text = "F";
			this.toolStripTextBox1.TextChanged += new System.EventHandler(this.toolStripTextBox1_TextChanged);
			// 
			// playbackToolStripMenuItem
			// 
			this.playbackToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.orderToolStripMenuItem,
            this.toolStripSeparator1,
            this.stopAfterCurrentToolStripMenuItem,
            this.playbackFollowsCursorToolStripMenuItem});
			this.playbackToolStripMenuItem.Name = "playbackToolStripMenuItem";
			this.playbackToolStripMenuItem.Size = new System.Drawing.Size(127, 44);
			this.playbackToolStripMenuItem.Text = "Playback";
			// 
			// orderToolStripMenuItem
			// 
			this.orderToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.defaultToolStripMenuItem,
            this.repeatPlaylistToolStripMenuItem,
            this.repeatTrackToolStripMenuItem,
            this.shuffleToolStripMenuItem});
			this.orderToolStripMenuItem.Name = "orderToolStripMenuItem";
			this.orderToolStripMenuItem.Size = new System.Drawing.Size(403, 44);
			this.orderToolStripMenuItem.Text = "Order";
			this.orderToolStripMenuItem.DropDownItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.orderToolStripMenuItem_DropDownItemClicked);
			// 
			// toolStripSeparator1
			// 
			this.toolStripSeparator1.Name = "toolStripSeparator1";
			this.toolStripSeparator1.Size = new System.Drawing.Size(400, 6);
			// 
			// stopAfterCurrentToolStripMenuItem
			// 
			this.stopAfterCurrentToolStripMenuItem.Checked = true;
			this.stopAfterCurrentToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
			this.stopAfterCurrentToolStripMenuItem.Name = "stopAfterCurrentToolStripMenuItem";
			this.stopAfterCurrentToolStripMenuItem.Size = new System.Drawing.Size(403, 44);
			this.stopAfterCurrentToolStripMenuItem.Text = "Stop After Current";
			this.stopAfterCurrentToolStripMenuItem.Click += new System.EventHandler(this.stopAfterCurrentToolStripMenuItem_Click);
			// 
			// playbackFollowsCursorToolStripMenuItem
			// 
			this.playbackFollowsCursorToolStripMenuItem.Checked = true;
			this.playbackFollowsCursorToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
			this.playbackFollowsCursorToolStripMenuItem.Name = "playbackFollowsCursorToolStripMenuItem";
			this.playbackFollowsCursorToolStripMenuItem.Size = new System.Drawing.Size(403, 44);
			this.playbackFollowsCursorToolStripMenuItem.Text = "Playback Follows Cursor";
			this.playbackFollowsCursorToolStripMenuItem.Click += new System.EventHandler(this.playbackFollowsCursorToolStripMenuItem_Click);
			// 
			// tabControl1
			// 
			this.tabControl1.Controls.Add(this.tabPage2);
			this.tabControl1.Controls.Add(this.tabPage1);
			this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tabControl1.Location = new System.Drawing.Point(0, 48);
			this.tabControl1.Margin = new System.Windows.Forms.Padding(6);
			this.tabControl1.Name = "tabControl1";
			this.tabControl1.SelectedIndex = 0;
			this.tabControl1.Size = new System.Drawing.Size(1164, 760);
			this.tabControl1.TabIndex = 23;
			// 
			// tabPage2
			// 
			this.tabPage2.Controls.Add(this.checkBox_AutoOctave);
			this.tabPage2.Controls.Add(this.panel_ProcessSelector);
			this.tabPage2.Controls.Add(this.panel2);
			this.tabPage2.Controls.Add(this.panel1);
			this.tabPage2.Location = new System.Drawing.Point(8, 40);
			this.tabPage2.Margin = new System.Windows.Forms.Padding(6);
			this.tabPage2.Name = "tabPage2";
			this.tabPage2.Padding = new System.Windows.Forms.Padding(6);
			this.tabPage2.Size = new System.Drawing.Size(1148, 712);
			this.tabPage2.TabIndex = 1;
			this.tabPage2.Text = "Midi Player";
			this.tabPage2.UseVisualStyleBackColor = true;
			// 
			// checkBox_AutoOctave
			// 
			this.checkBox_AutoOctave.AutoSize = true;
			this.checkBox_AutoOctave.Checked = true;
			this.checkBox_AutoOctave.CheckState = System.Windows.Forms.CheckState.Checked;
			this.checkBox_AutoOctave.Location = new System.Drawing.Point(346, 22);
			this.checkBox_AutoOctave.Margin = new System.Windows.Forms.Padding(18, 16, 18, 16);
			this.checkBox_AutoOctave.Name = "checkBox_AutoOctave";
			this.checkBox_AutoOctave.Size = new System.Drawing.Size(214, 30);
			this.checkBox_AutoOctave.TabIndex = 23;
			this.checkBox_AutoOctave.Text = "Auto Octave Shift";
			this.checkBox_AutoOctave.UseVisualStyleBackColor = true;
			this.checkBox_AutoOctave.CheckedChanged += new System.EventHandler(this.checkBox_AutoOctave_CheckedChanged);
			// 
			// panel_ProcessSelector
			// 
			this.panel_ProcessSelector.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.panel_ProcessSelector.Controls.Add(this.label4);
			this.panel_ProcessSelector.Controls.Add(this.comboBox_GameProcesses);
			this.panel_ProcessSelector.Controls.Add(this.button_Ident);
			this.panel_ProcessSelector.Location = new System.Drawing.Point(798, 6);
			this.panel_ProcessSelector.Margin = new System.Windows.Forms.Padding(6);
			this.panel_ProcessSelector.Name = "panel_ProcessSelector";
			this.panel_ProcessSelector.Size = new System.Drawing.Size(350, 70);
			this.panel_ProcessSelector.TabIndex = 22;
			// 
			// label4
			// 
			this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.label4.AutoSize = true;
			this.label4.Location = new System.Drawing.Point(0, 18);
			this.label4.Margin = new System.Windows.Forms.Padding(14, 0, 14, 0);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(96, 26);
			this.label4.TabIndex = 20;
			this.label4.Text = "Game Id";
			// 
			// tabPage1
			// 
			this.tabPage1.Controls.Add(this.button_Monitor);
			this.tabPage1.Controls.Add(this.comboBoxMidiInDevices);
			this.tabPage1.Controls.Add(this.label1);
			this.tabPage1.Controls.Add(this.checkBox_ArpeggiateUp);
			this.tabPage1.Controls.Add(this.numericUpDown_SemitoneShift);
			this.tabPage1.Location = new System.Drawing.Point(8, 40);
			this.tabPage1.Margin = new System.Windows.Forms.Padding(6);
			this.tabPage1.Name = "tabPage1";
			this.tabPage1.Padding = new System.Windows.Forms.Padding(6);
			this.tabPage1.Size = new System.Drawing.Size(1148, 720);
			this.tabPage1.TabIndex = 0;
			this.tabPage1.Text = "Keyboard";
			this.tabPage1.UseVisualStyleBackColor = true;
			// 
			// defaultToolStripMenuItem
			// 
			this.defaultToolStripMenuItem.Checked = true;
			this.defaultToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
			this.defaultToolStripMenuItem.Name = "defaultToolStripMenuItem";
			this.defaultToolStripMenuItem.Size = new System.Drawing.Size(316, 44);
			this.defaultToolStripMenuItem.Tag = AutoBard.Enums.Playback.Default;
			this.defaultToolStripMenuItem.Text = "Default";
			// 
			// repeatPlaylistToolStripMenuItem
			// 
			this.repeatPlaylistToolStripMenuItem.Name = "repeatPlaylistToolStripMenuItem";
			this.repeatPlaylistToolStripMenuItem.Size = new System.Drawing.Size(316, 44);
			this.repeatPlaylistToolStripMenuItem.Tag = AutoBard.Enums.Playback.RepeatPlaylist;
			this.repeatPlaylistToolStripMenuItem.Text = "Repeat (Playlist)";
			// 
			// repeatTrackToolStripMenuItem
			// 
			this.repeatTrackToolStripMenuItem.Name = "repeatTrackToolStripMenuItem";
			this.repeatTrackToolStripMenuItem.Size = new System.Drawing.Size(316, 44);
			this.repeatTrackToolStripMenuItem.Tag = AutoBard.Enums.Playback.RepeatTrack;
			this.repeatTrackToolStripMenuItem.Text = "Repeat (Track)";
			// 
			// shuffleToolStripMenuItem
			// 
			this.shuffleToolStripMenuItem.Name = "shuffleToolStripMenuItem";
			this.shuffleToolStripMenuItem.Size = new System.Drawing.Size(316, 44);
			this.shuffleToolStripMenuItem.Tag = AutoBard.Enums.Playback.Shuffle;
			this.shuffleToolStripMenuItem.Text = "Shuffle";
			// 
			// AutoBardForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(192F, 192F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
			this.ClientSize = new System.Drawing.Size(1164, 808);
			this.Controls.Add(this.tabControl1);
			this.Controls.Add(this.menuStrip1);
			this.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.MainMenuStrip = this.menuStrip1;
			this.Margin = new System.Windows.Forms.Padding(18, 16, 18, 16);
			this.Name = "AutoBardForm";
			this.Text = "AutoBard";
			this.Load += new System.EventHandler(this.Form1_Load);
			((System.ComponentModel.ISupportInitialize)(this.numericUpDown_SemitoneShift)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
			this.panel1.ResumeLayout(false);
			this.panel1.PerformLayout();
			this.panel2.ResumeLayout(false);
			this.panel2.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.trackBar1)).EndInit();
			this.menuStrip1.ResumeLayout(false);
			this.menuStrip1.PerformLayout();
			this.tabControl1.ResumeLayout(false);
			this.tabPage2.ResumeLayout(false);
			this.tabPage2.PerformLayout();
			this.panel_ProcessSelector.ResumeLayout(false);
			this.panel_ProcessSelector.PerformLayout();
			this.tabPage1.ResumeLayout(false);
			this.tabPage1.PerformLayout();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.ComboBox comboBoxMidiInDevices;
		private System.Windows.Forms.Button button_Monitor;
		private System.Windows.Forms.CheckBox checkBox_ArpeggiateUp;
		private System.Windows.Forms.NumericUpDown numericUpDown_SemitoneShift;
		private System.Windows.Forms.TextBox textBox_MidiDirectory;
		private System.Windows.Forms.Button button_Play;
		private System.Windows.Forms.Button button_Browse;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.DataGridView dataGridView1;
		private System.Windows.Forms.DataGridViewTextBoxColumn notesDataGridViewTextBoxColumn;
		private System.Windows.Forms.Button button_Refresh;
		private System.Windows.Forms.ComboBox comboBox_GameProcesses;
		private System.Windows.Forms.Button button_Ident;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.Panel panel2;
		private System.Windows.Forms.TextBox textBox_Filter;
		private System.Windows.Forms.Button button1;
		private System.Windows.Forms.MenuStrip menuStrip1;
		private System.Windows.Forms.ToolStripMenuItem menuToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem alwaysOnTopToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
		private System.Windows.Forms.TabControl tabControl1;
		private System.Windows.Forms.TabPage tabPage2;
		private System.Windows.Forms.TabPage tabPage1;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Panel panel_ProcessSelector;
		private System.Windows.Forms.CheckBox checkBox_AutoOctave;
		private System.Windows.Forms.DataGridViewTextBoxColumn nameDataGridViewTextBoxColumn;
		private System.Windows.Forms.DataGridViewTextBoxColumn Notes;
		private System.Windows.Forms.DataGridViewTextBoxColumn semitoneShiftDataGridViewTextBoxColumn;
		private System.Windows.Forms.DataGridViewCheckBoxColumn arpeggiateUpDataGridViewCheckBoxColumn;
		private System.Windows.Forms.DataGridViewButtonColumn Ensemble;
		private System.Windows.Forms.ToolStripMenuItem toolsToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem aFKModeToolStripMenuItem;
		private System.Windows.Forms.Label label_ProgressTime;
		private System.Windows.Forms.TrackBar trackBar1;
		private System.Windows.Forms.ToolStripTextBox toolStripTextBox1;
		private System.Windows.Forms.ToolStripMenuItem playbackToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem stopAfterCurrentToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem playbackFollowsCursorToolStripMenuItem;
		private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
		private System.Windows.Forms.ToolStripMenuItem shuffleToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem repeatTrackToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem repeatPlaylistToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem defaultToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem orderToolStripMenuItem;
	}
}

