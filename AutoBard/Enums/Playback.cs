﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutoBard.Enums
{
	[Flags]
	public enum Playback
	{
		Default				= 0,
		RepeatPlaylist		= 1 << 0,
		RepeatTrack			= 1 << 1,
		Shuffle				= 1 << 2,
	}
}
