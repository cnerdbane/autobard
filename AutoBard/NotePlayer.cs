﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AutoBard
{
	public class NotePlayer
	{
		private const int GLOBAL_SEMITONE_SHIFT = -12;
		private const int MINIMUM_INTERVAL = 1;
		private object _playLock = new object();

		private readonly List<ConcurrentPriorityQueue<int, int>> noteQueue;
		private List<int> playingNotes;
		private readonly int minNote;
		private readonly int maxNote;
		private readonly Dictionary<int, Keys> notes = new Dictionary<int, Keys>
		{
			{ 36, Keys.Z },  // C3
			{ 37, Keys.O },  // C#
			{ 38, Keys.X },  // D
			{ 39, Keys.P },  // Eb
			{ 40, Keys.C },  // E
			{ 41, Keys.V },  // F
			{ 42, Keys.K },  // F#
			{ 43, Keys.B },  // G
			{ 44, Keys.L },  // G#
			{ 45, Keys.N },  // A
			{ 46, Keys.OemPeriod }, // Bb
			{ 47, Keys.M },  // B
			{ 48, Keys.A },  // C4
			{ 49, Keys.D6 }, // C#
			{ 50, Keys.S },  // D
			{ 51, Keys.D7 }, // Eb
			{ 52, Keys.D },  // E
			{ 53, Keys.F },  // F
			{ 54, Keys.D8 }, // F#
			{ 55, Keys.G },  // G
			{ 56, Keys.D9 }, // G#
			{ 57, Keys.H },  // A
			{ 58, Keys.D0 }, // Bb
			{ 59, Keys.J },  // B
			{ 60, Keys.Q },  // C5
			{ 61, Keys.D1 }, // C#
			{ 62, Keys.W },  // D
			{ 63, Keys.D2 }, // Eb
			{ 64, Keys.E },  // E
			{ 65, Keys.R },  // F
			{ 66, Keys.D3 }, // F#
			{ 67, Keys.T },  // G
			{ 68, Keys.D4 }, // G#
			{ 69, Keys.Y },  // A
			{ 70, Keys.D5 }, // Bb
			{ 71, Keys.U },  // B
			{ 72, Keys.I },  // C6
		};

		public NotePlayer(int channelCount)
		{
			this.minNote = this.notes.Keys.Min();
			this.maxNote = this.notes.Keys.Max();
			this.noteQueue = new List<ConcurrentPriorityQueue<int, int>>(Enumerable.Range(0, channelCount).Select(i => new ConcurrentPriorityQueue<int, int>()));
			this.playingNotes = Enumerable.Range(0, channelCount).ToList();
		}

		public int SemitoneShift { get; set; }
		public bool ArpeggiateUp { get; set; }
		public bool Paused { get; set; }
		public bool AutoOctaveShift { get; set; }

		public void PlayNote(int note, int players, bool on)
		{
			note += this.SemitoneShift + GLOBAL_SEMITONE_SHIFT;

			if (false == this.notes.ContainsKey(note))
			{
				if (this.AutoOctaveShift)
				{
					while (note > this.maxNote) note -= 12;
					while (note < this.minNote) note += 12;
				}
				else
				{
					return;
				}
			}

			// loop through all possible players
			for (int playerIndex = 0; playerIndex < this.noteQueue.Count; playerIndex++)
			{
				// skip players that don't play this note
				if ((players & (1 << playerIndex)) == 0) continue;

				// release note immediately
				if (false == on)
				{
					GameInput.PostRelease(this.notes[note], playerIndex);
				}
				else
				{
					while (this.Paused)
					{
						Thread.Sleep(100); // todo: this is dumb i am shame
					}

					// use the numeric value of the midi note as its priority
					int priority = this.ArpeggiateUp ? note : -note;
					this.noteQueue[playerIndex].Enqueue(priority, note);

					// i think a race condition is happening here where playerIndex is incremented before the Task starts
					int _playerIndex = playerIndex;
					Task.Run(() => this.HandleNextNotes(_playerIndex));
				}
			}
		}

		private void HandleNextNotes(int playerIndex)
		{
			Thread.Sleep(1); // todo: this is dumb i am shame, allow time to queue chords
			lock (_playLock)
			{
				while (!this.noteQueue[playerIndex].IsEmpty)
				{
					// release previous note. ffxiv can only play one at a time
					if (this.notes.ContainsKey(this.playingNotes[playerIndex]))
					{
						GameInput.PostRelease(this.notes[this.playingNotes[playerIndex]], playerIndex);
					}

					if (this.noteQueue[playerIndex].TryDequeue(out KeyValuePair<int, int> result))
					{
						int note = result.Value;
						this.playingNotes[playerIndex] = note;

						GameInput.PostRelease(this.notes[this.playingNotes[playerIndex]], playerIndex);
						Thread.Sleep(5); // give it time to release the key for repeated notes
						GameInput.PostHold(this.notes[this.playingNotes[playerIndex]], playerIndex);

						Thread.Sleep(MINIMUM_INTERVAL); // dumb temp hack for minimum ffxiv note interval?
					}
				}
			}
		}
	}
}
