﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AutoBard.Extensions
{
	public static class DataRowExtensions
	{
		public static T ToObject<T>(this DataRow row) where T : new()
		{
			PropertyDescriptorCollection propertyDescriptorCollection = TypeDescriptor.GetProperties(typeof(T));

			T item = new T();

			foreach (DataColumn column in row.Table.Columns)
			{
				string columnName = column.ColumnName;
				object value = row.Field<object>(column);
				if (value == DBNull.Value) value = null;
				if (false == (propertyDescriptorCollection.Find(columnName, false) is null))
				{
					propertyDescriptorCollection[columnName].SetValue(item, value);
				}
			}

			return item;
		}
	}
}
