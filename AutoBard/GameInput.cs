﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AutoBard
{
	public static class GameInput
	{
		public static IntPtr[] GameHandles { get; private set; }

        static GameInput()
		{
            RefreshGameHandles();
        }

        public static void RefreshGameHandles()
        {
			// todo: maybe more reliable way to get game process
			Process[] processes = Process.GetProcessesByName("ffxiv_dx11");
			GameHandles = processes.Select(p => p.MainWindowHandle).ToArray();
        }

        public static async Task SendHold(Keys key, int delay = 25, int handleIndex = -1)
        {
            IntPtr gameHandle;
            if (handleIndex == -1) gameHandle = GameHandles.First();
            else gameHandle = GameHandles[handleIndex % GameHandles.Length];

            Keys baseKey = key & ~Keys.Control & key & ~Keys.Shift & key & ~Keys.Alt;

            if (baseKey != key)
            {
                if ((key & Keys.Control) == Keys.Control) NativeMethods.SendMessage(gameHandle, NativeMethods.WM_KEYDOWN, (IntPtr)Keys.ControlKey, (IntPtr)0);
                if ((key & Keys.Alt) == Keys.Alt) NativeMethods.SendMessage(gameHandle, NativeMethods.WM_SYSKEYDOWN, (IntPtr)Keys.Menu, (IntPtr)0);
                if ((key & Keys.Shift) == Keys.Shift) NativeMethods.SendMessage(gameHandle, NativeMethods.WM_KEYDOWN, (IntPtr)Keys.ShiftKey, (IntPtr)0);
                await Task.Delay(25);
            }

            NativeMethods.SendMessage(gameHandle, NativeMethods.WM_KEYDOWN, (IntPtr)baseKey, (IntPtr)0);
            await Task.Delay(delay);
        }

        public static async Task SendRelease(Keys key, int delay = 25, int handleIndex = -1)
        {
            IntPtr gameHandle;
            if (handleIndex == -1) gameHandle = GameHandles.First();
            else gameHandle = GameHandles[handleIndex % GameHandles.Length];

            Keys baseKey = key & ~Keys.Control & key & ~Keys.Shift & key & ~Keys.Alt;

            NativeMethods.SendMessage(gameHandle, NativeMethods.WM_KEYUP, (IntPtr)baseKey, (IntPtr)0);

            if (baseKey != key)
            {
                if ((key & Keys.Shift) == Keys.Shift)
                {
                    await Task.Delay(5);
                    NativeMethods.SendMessage(gameHandle, NativeMethods.WM_KEYUP, (IntPtr)Keys.ShiftKey, (IntPtr)0);
                }

                if ((key & Keys.Alt) == Keys.Alt)
                {
                    await Task.Delay(5);
                    NativeMethods.SendMessage(gameHandle, NativeMethods.WM_SYSKEYUP, (IntPtr)Keys.Menu, (IntPtr)0);
                }

                if ((key & Keys.Control) == Keys.Control)
                {
                    await Task.Delay(5);
                    NativeMethods.SendMessage(gameHandle, NativeMethods.WM_KEYUP, (IntPtr)Keys.ControlKey, (IntPtr)0);
                }
            }

            await Task.Delay(delay);
        }

        public static async Task SendPress(Keys key, int downDelay = 25, int upDelay = 25, int handleIndex = -1)
        {
            await SendHold(key, downDelay, handleIndex);
            await SendRelease(key, upDelay, handleIndex);
        }

        public static void PostHold(Keys key, int handleIndex)
        {
            IntPtr gameHandle;
            if (handleIndex == -1) gameHandle = GameHandles.First();
            else gameHandle = GameHandles[handleIndex % GameHandles.Length];

            if (key != (key & ~Keys.Control & key & ~Keys.Shift & key & ~Keys.Alt))
                throw new Exception("ASync Key methods do not accept modifier keys.");

            NativeMethods.PostMessage(gameHandle, NativeMethods.WM_KEYDOWN, (IntPtr)key, (IntPtr)0);
        }

        public static void PostRelease(Keys key, int handleIndex)
        {
            IntPtr gameHandle;
            if (handleIndex == -1) gameHandle = GameHandles.First();
            else gameHandle = GameHandles[handleIndex % GameHandles.Length];

            if (key != (key & ~Keys.Control & key & ~Keys.Shift & key & ~Keys.Alt))
                throw new Exception("ASync Key methods do not accept modifier keys.");

            NativeMethods.PostMessage(gameHandle, NativeMethods.WM_KEYUP, (IntPtr)key, (IntPtr)0);
        }

        public static async Task SendText(string text)
        {
            NativeMethods.CopyToClipboard(text);
            await SendPress(Keys.Enter, handleIndex: -1);
            await SendPress((int)Keys.Control + Keys.V, handleIndex: -1);
            await SendPress(Keys.Enter, handleIndex: -1);
        }
    }
}
