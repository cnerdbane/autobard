﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutoBard.Extensions
{
	public static class BitwiseExtensions
	{
		public static bool IsEnabled(int i, int bit)
		{
			int shiftedBit = getShiftedBit(bit);
			return (i & shiftedBit) == shiftedBit;
		}

		public static int Toggle(int i, int bit)
		{
			int shiftedBit = getShiftedBit(bit);
			return i ^= shiftedBit;
		}

		public static int Enable(int i, int bit)
		{
			int shiftedBit = getShiftedBit(bit);
			return i |= shiftedBit;
		}

		public static int Disable(int i, int bit)
		{
			int shiftedBit = getShiftedBit(bit);
			return i &= ~shiftedBit;
		}

		private static int getShiftedBit(int bit)
		{
			return 1 << bit;
		}
	}
}
