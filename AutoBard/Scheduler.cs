﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutoBard
{
    class Scheduler
    {
        private readonly ConcurrentDictionary<Action, ScheduledTask> _scheduledTasks = new ConcurrentDictionary<Action, ScheduledTask>();

        public async Task Execute(Action action, int timeoutMs)
        {
            if (timeoutMs > 0)
            {
                var task = new ScheduledTask(action, timeoutMs);
                task.TaskComplete += RemoveTask;
                _scheduledTasks.TryAdd(action, task);
                task.Timer.Start();
                try
                {
                    await Task.Delay(timeoutMs, task.CancellationToken);
                }
                catch (OperationCanceledException)
                {

                }
            }
            else
            {
                action?.Invoke();
            }
		}

		private void RemoveTask(object sender, EventArgs e)
        {
            var task = (ScheduledTask)sender;
            task.TaskComplete -= RemoveTask;
            ScheduledTask deleted;
            _scheduledTasks.TryRemove(task.Action, out deleted);
        }
    }
}
